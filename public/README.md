# Pt2a-CooperativaAdaptativa

Pt2a-CooperativaAdaptativa - HTML5+CSS3+Bootstrap per a un disseny adaptatiu

Tant el grid de productes, com el menú superior son 100% responsive.
Les tipografies també són 100% responsive.

Eliminar fons imatges (en cas que no volgueu usar Gimp o Photoshop):
[RemoveBG](https://www.remove.bg/upload)

Per a fer el Grid responsive recomano:
### [Exemples Grid Bootstrap](https://gitlab.com/m09uf1-introwebdesign/ejerciciosgridboostrap/-/tree/master)
### [Web botiga la Bajoca](https://botiga.labajoca.cat/)

Si voleu que el Grid ocupi el 100% de la pantalla:
### [Propietat mw-100, web Boostrap](https://getbootstrap.com/docs/4.0/utilities/sizing/)

Per a fer la barra de menú:
### [Menu pàgina Ecommerce](https://mdbootstrap.com/docs/b4/jquery/ecommerce/layout/navbar/)
### [Exemple navbar Moodle](https://campus.proven.cat/mod/resource/view.php?id=157682)
### [Documentació Boostrap, Nav & Tabs](https://getbootstrap.com/docs/5.0/forms/form-control/)
### [Codepen, menú pàgina responsive](https://codepen.io/SitePoint/pen/rJewLG)

Per afegir un formulari d'alta:
### [Exemple formularis Moodle](https://campus.proven.cat/mod/resource/view.php?id=157682)

